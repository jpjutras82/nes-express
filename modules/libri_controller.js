// FORMATIONS CONTROLS
class formation {
    constructor(id, name, description, cout, type) {
        this.id = id
        this.name = name
        this.description = description
        this.cout = cout
        this.type = type
    }
}

const formations = [
    new formation(0, "MongoDB 1", "Lorem ipsum dolor sit amet.", "16$", "mongodb"),
    new formation(1, "MongoDB 2", "Lorem ipsum dolor sit amet.", "24$", "mongodb"),
    new formation(2, "MongoDB 3", "Lorem ipsum dolor sit amet.", "16$", "mongodb"),
    new formation(3, "MongoDB 4", "Lorem ipsum dolor sit amet.", "32$", "mongodb"),
    new formation(4, "MongoDB 5", "Lorem ipsum dolor sit amet.", "12$", "mongodb"),
    new formation(5, "Nodejs 1", "Lorem ipsum dolor sit amet.", "16$", "nodejs"),
    new formation(6, "Nodejs 2", "Lorem ipsum dolor sit amet.", "24$", "nodejs"),
    new formation(7, "Nodejs 3", "Lorem ipsum dolor sit amet.", "16$", "nodejs"),
    new formation(8, "Nodejs 4", "Lorem ipsum dolor sit amet.", "32$", "nodejs"),
    new formation(9, "Nodejs 5", "Lorem ipsum dolor sit amet.", "12$", "nodejs")
]
exports.allFormations = function(){
    return formations
}

exports.getFormationByType = function(type){
        var formationsByType = []
        for (let i=0; i<formations.length; i++){
            if((formations[i].type) == (type))
            formationsByType.push(formations[i])
        }
        return formationsByType
      }
      
exports.getFormationById = function(id) {
    return formations[id]
}


    
// BLOG & ARTICLES CONTROLS
class article {
    constructor(id, title, content, author, date) {
        this.id = id
        this.title = title
        this.content = content
        this.author = author
        this.date = date
    }
}

const blog = [
    new article(0, "Article 1", "Lorem ipsum dolor sit amet.", "Jp Jutras", "1999/03/20"),
    new article(1, "Article 2", "Lorem ipsum dolor sit amet.", "Jp Lambert", "2004/05/10"),
    new article(2, "Article 3", "Lorem ipsum dolor sit amet.", "Jp Jutras", "2010/05/12"),
    new article(3, "Article 4", "Lorem ipsum dolor sit amet.", "Julie Jutras", "1998/05/10"),
    new article(4, "Article 5", "Lorem ipsum dolor sit amet.", "Joanie Gauthier", "2018/01/01"),
    new article(5, "Article 6", "Lorem ipsum dolor sit amet.", "Audrey Fortin Rioux", "2019/02/05"),
    new article(6, "Article 7", "Lorem ipsum dolor sit amet.", "Patrick Fleury", "2020/03/13"),
    new article(7, "Article 8", "Lorem ipsum dolor sit amet.", "Lenore", "2020/04/12"),
    new article(8, "Article 9", "Lorem ipsum dolor sit amet.", "Edgar Allan Poe", "1809/01/19"),
    new article(9, "Article 10", "Lorem ipsum dolor sit amet.", "Floria Sigismondi", "1996/01/01")
]

exports.getArticleById = function(id) {
    return blog[id]
}

exports.allarticles = blog

//CONTACT CONTROLS
class dataContact {
    constructor(id, lastname, firstname, email, tel, subject, message) {
        this.id = id
        this.lastname = lastname
        this.firstname = firstname
        this.email = email
        this.tel = tel
        this.subject = subject
        this.message = message
    }
}

const dataContactUser = [
    new dataContact(0, "Martel", "Joel", "joelmartel@hotmail.com", "(514) 444-0494", "Nouvel étudiant", "Patati, patata!")
]
 
exports.saveDataContact = function(data){
    let id = dataContactUser.length
    dataContactUser.push(new dataContact(id, data.lastname, data.firstname, data.email, data.tel, data.subject, data.message))
    return true
}

exports.contactInformation = function(){
    return dataContactUser
}