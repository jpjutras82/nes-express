var express = require('express');
var router = express.Router();
var libri_controller = require("../modules/libri_controller")

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'LIBRI - Plateforme de formations en ligne',
    title2: "Bienvenue sur la plateforme de formation LIBRI!",
  });
});

router.get('/formations/formations', function(req,res,next){
  res.render('./formations/formations',{
    title: "LIBRI - Plateforme de formations en ligne",
    title2: "Toutes nos formations",
    formation: libri_controller.allFormations()
  })
})

router.get('/formations/:type', function(req,res,next){
  res.render('./formations/formations',{
    title: "LIBRI - Plateforme de formations en ligne",
    title2: "Nos formations",
    formation: libri_controller.getFormationByType(req.params.type)
  })
})

router.get("/formations/formation/:id", function (req, res, next) {
  res.render("./formations/formation", {
    title: "LIBRI - Plateforme de formations en ligne",
    title2: "Nos formations",
    formation: libri_controller.getFormationById(req.params.id) })
})

router.get('/blog/blog', function (req, res, next) {
  res.render('./blog/blog', {
    title: "LIBRI - Plateforme de formations en ligne",
    title2: "blog",
    blog: libri_controller.allarticles });
});

router.get("/blog/:id", function (req, res, next) {
  res.render("./blog/article", {
    title: "LIBRI - Plateforme de formations en ligne",
    title2: "Blog",
    article: libri_controller.getArticleById(req.params.id) })
})

router.get("/contact", function (req, res, next) {
  res.render("contact", {
    title: "LIBRI - Plateforme de formations en ligne",
    title2: "Nous contacter",
    formation: libri_controller.contactInformation()
  })
})

router.post("/contact", function (req, res, next) {
  console.log(req.body)
  libri_controller.saveDataContact(req.body)
  res.render("./contact", {
    title: "LIBRI - Plateforme de formations en ligne",
    title2: "Nous contacter",
  })
})

module.exports = router;